Building
========

Libmegapixels is build through Meson. the Cmake files present in the repository are
only for IDE integration and does not install the binaries and required files.

The libmegapixels library depends on libconfig and linux-headers only.

The build steps for libmegapixels is just the normal Meson procedure for
building and installing a library:

.. code-block:: shell-session

   $ git clone https://gitlab.com/megapixels-org/libmegapixels.git
   Switch over to your favorite release branch after this
   $ meson setup build
   The Meson build system
   Version: 1.3.1
   ... etcetera
   $ cd build
   $ meson compile
   compiler output here...
   $ sudo meson install
   Installing libmegapixels.so.1.0.0 to /usr/local/lib
   Installing megapixels-findconfig to /usr/local/bin
   Installing megapixels-getframe to /usr/local/bin
   Installing /..snip.../include/libmegapixels.h to /usr/local/include
   Installing /..snip.../libmegapixels.pc to /usr/local/lib/pkgconfig
   Installing /..snip.../config/pine64,pinephone.conf to /usr/local/share/megapixels/config
   Installing symlink pointing to libmegapixels.so.1.0.0 to /usr/local/lib/libmegapixels.so.1
   Installing symlink pointing to libmegapixels.so.1 to /usr/local/lib/libmegapixels.so

This will install the :code:`libmegapixels.so.1.0.0` library globally. This usually
ends up in :code:`/usr/local/lib` which is not by default in the search path for
libraries.

This will also install the libmegapixels utilities and bundled config files.