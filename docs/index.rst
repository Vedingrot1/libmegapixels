.. libmegapixels documentation master file, created by
   sphinx-quickstart on Sun Dec  3 14:27:28 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

libmegapixels's documentation!
==============================

The libmegapixels library is an abstraction library between the V4L2 system in
linux and the Megapixels GTK application. It helps with abstracting away the
media graph interface in Linux for ARM platforms with cameras.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   building
   overview
   faq